const data = require("./funds.json")


function nooffundhouses(data){
    let obj ={};
    for(let i=0;i<data.length;i++){
        if (data[i].fund_house in obj){
            obj[data[i].fund_house] +=1
        }else{
            obj[data[i].fund_house] =1
        }
    }
    return obj;
}



function category(data){
    let obj = {};
    for (let i=0;i<data.length;i++){
        if (data[i].fund_house in obj){
            if (data[i].category in obj[data[i].fund_house]){
                obj[data[i].fund_house][data[i].category] +=1
            }else{
                obj[data[i].fund_house][data[i].category] =1
            }
        }else{
            obj[data[i].fund_house] ={}
            obj[data[i].fund_house][data[i].category] =1
        }
    }
    return obj;
}
//&& obj[data[i].returns.year_1]
//{fund_house:{sum:0,count:0}}
function avgreturns(data){
    let obj={} ;
    for (let i=0;i<data.length;i++){
        if (!data[i].returns.year_1){
            continue
        }
        if (data[i].fund_house in obj){
            obj[data[i].fund_house]["sum"] += data[i].returns.year_1
            obj[data[i].fund_house]["count"] +=1
        }else{
            obj[data[i].fund_house] ={sum:0,count:0}
            obj[data[i].fund_house]["sum"]= data[i].returns.year_1
            obj[data[i].fund_house]["count"] =1
        }
    }
    let obj1={}
    for (let i in obj){
        obj1[i] = parseFloat((obj[i]["sum"]/obj[i]["count"]).toFixed(3))
    }
    return obj1;
}

console.log(avgreturns(data));